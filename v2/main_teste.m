clc; clear; format shortg;close all;

w = logspace(-1,4);

circuito1 = circuito_class();
%-------cria os bipolos-----------------------------
%n� 0 � o GND
%o primeiro n� � onde liga o '-', o segundo � o '+'

R1 = bipolo_class('R1',bipolo_class.RES,[1 2],1,[]);
R2 = bipolo_class('R2',bipolo_class.RES,[2 3],8,[]);
C1 = bipolo_class('C1',bipolo_class.CAP,[0 2],0.2,[]);
C2 = bipolo_class('C2',bipolo_class.CAP,[0 3],0.1,[]);

%-----------adiciona os bipolos no circuito---------

circuito1 = circuito1.add_bipolo(R1);
circuito1 = circuito1.add_bipolo(R2);
circuito1 = circuito1.add_bipolo(C1);
circuito1 = circuito1.add_bipolo(C2);

circuito1.in = [0 1];
circuito1.out = [0 3];
%--------------------------------------------------
load('FRA.mat')
disp(avaliar_resposta(circuito1,pontos_esp));
% 
% V1 = bipolo_class('Vin',bipolo_class.Vin,circuito1.in,1,[]);
% circuito1 = circuito1.add_bipolo(V1);
% 
% L1 = bipolo_class('L1',bipolo_class.IND,[0 3],0.1,[]);
% 
% figure(1)
% desenha_bipolo(V1,[0 0],[1 1]);
% desenha_bipolo(V1,[0 1],[0 2]);
% desenha_bipolo(V1,[1 0],[2 0]);
% grid on
% xlim([-2 2]);
% ylim([-2 2]);