function desenha_bipolo(bipolo,p1,p2)
hold on
angulo = atan2(p2(2)-p1(2),p2(1)-p1(1));
p_central = [(p1(1)+p2(1))/2 (p1(2)+p2(2))/2];

switch(bipolo.tipo)
    case bipolo.RES
        textx = 0;
        texty = 0.375;
        [textx,texty] = mov_rot(textx,texty,p_central,angulo) ;
        text(textx, texty,bipolo.nome, 'HorizontalAlignment', 'Center', 'Color',[0 0 0]);
        
        textx = 0;
        texty = -0.375;
        [textx,texty] = mov_rot(textx,texty,p_central,angulo) ;
        text(textx, texty,num2str(bipolo.valor), 'HorizontalAlignment', 'Center', 'Color',[0 0 0]);
        
        x = [-0.5 -0.25 -0.187 -0.062 0.062 0.187 0.25 0.5];
        y = [0 0 0.2 -0.2 0.2 -0.2 0 0];
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        x(1) = p1(1);
        x(8) = p2(1);
        y(1) = p1(2);
        y(8) = p2(2);
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
    case bipolo.CAP
        textx = 0;
        texty = 0.375;
        [textx,texty] = mov_rot(textx,texty,p_central,angulo) ;
        text(textx, texty,bipolo.nome, 'HorizontalAlignment', 'Center', 'Color',[0 0 0]);
        
        textx = 0;
        texty = -0.375;
        [textx,texty] = mov_rot(textx,texty,p_central,angulo) ;
        text(textx, texty,num2str(bipolo.valor), 'HorizontalAlignment', 'Center', 'Color',[0 0 0]);
        
        x = [-0.5 -0.07 -0.07 -0.07];
        y = [0 0 0.2 -0.2];
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        x(1) = p1(1);
        y(1) = p1(2);
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
        
        x = [0.07 0.07 0.07 0.5];
        y = [0.2 -0.2 0 0];
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        x(4) = p2(1);
        y(4) = p2(2);
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
    case bipolo.IND
        textx = 0;
        texty = 0.275;
        [textx,texty] = mov_rot(textx,texty,p_central,angulo) ;
        text(textx, texty,bipolo.nome, 'HorizontalAlignment', 'Center', 'Color',[0 0 0]);
        
        textx = 0;
        texty = -0.175;
        [textx,texty] = mov_rot(textx,texty,p_central,angulo) ;
        text(textx, texty,num2str(bipolo.valor), 'HorizontalAlignment', 'Center', 'Color',[0 0 0]);
        
        teta = 0:0.2:pi;
        R = 0.1;  %or whatever radius you want
        x = R*cos(teta) + 0.15;
        y = R*sin(teta);
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
        x = R*cos(teta);
        y = R*sin(teta);
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
        x = R*cos(teta) - 0.15;
        y = R*sin(teta);
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
        
        x = [-0.5 -0.25];
        y = [0 0 ];
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        x(1) = p1(1);
        y(1) = p1(2);
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
        
        x = [0.25 0.5];
        y = [0 0 ];
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        x(2) = p2(1);
        y(2) = p2(2);
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
    case bipolo.Vin
        textx = 0.375;
        texty = 0.175;
        [textx,texty] = mov_rot(textx,texty,p_central,angulo) ;
        text(textx, texty,bipolo.nome, 'HorizontalAlignment', 'Center', 'Color',[0 0 0]);
        
        textx = -0.375;
        texty = -0.175;
        [textx,texty] = mov_rot(textx,texty,p_central,angulo) ;
        text(textx, texty,num2str(bipolo.valor), 'HorizontalAlignment', 'Center', 'Color',[0 0 0]);
        
        teta = 0:0.2:2*pi;
        R = 0.25;  %or whatever radius you want
        x = R*cos(teta);
        y = R*sin(teta);
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
       
        
        x = [-0.5 -0.25];
        y = [0 0 ];
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        x(1) = p1(1);
        y(1) = p1(2);
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
        
        x = [-0.15 -0.05];
        y = [0 0 ];
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
        
        x = [0.05 0.15 0.1 0.1 0.1];
        y = [0 0 0 0.05 -0.05];
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
        
        x = [0.25 0.5];
        y = [0 0 ];
        [x,y] = mov_rot(x,y,p_central,angulo) ;
        x(2) = p2(1);
        y(2) = p2(2);
        plot(x,y,'Color',[0 0 1], 'LineWidth',2);
end

hold off
end

