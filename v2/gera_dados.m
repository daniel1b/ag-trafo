clc; clear; format shortg;close all;

w_esp = logspace(-1,4);

mod = 1:length(w_esp);
arg = 1:length(w_esp);
for k = 1:length(w_esp)
    circuito1 = circuito_class();
    %-------cria os bipolos-----------------------------
    %n� 0 � o GND
    %o primeiro n� � onde liga o '-', o segundo � o '+'
    Vc1 = bipolo_class(bipolo_class.Vin,[0 1],1,[]);
    
    R1 = bipolo_class(bipolo_class.RES,[1 2],1,[]);
    R2 = bipolo_class(bipolo_class.RES,[2 3],1,[]);
   
    C1 = bipolo_class(bipolo_class.CAP,[0 2],1/(1i*w_esp(k)*0.1),[]);
    C2 = bipolo_class(bipolo_class.CAP,[0 3],1/(1i*w_esp(k)*0.1),[]);
    
   %-----------adiciona os bipolos no circuito---------
    
    circuito1 = circuito1.add_bipolo(Vc1);
    circuito1 = circuito1.add_bipolo(R1);
    circuito1 = circuito1.add_bipolo(R2);
    circuito1 = circuito1.add_bipolo(C1);
    circuito1 = circuito1.add_bipolo(C2);

    
    %--------------------------------------------------
    tensoes = circuito1.get_tensoes();
    mod_esp(k) = 20*log10(abs(tensoes(3,1)));
    arg_esp(k) = (180/pi)*angle(tensoes(3,1));
end
pontos_esp.mod = mod_esp;
pontos_esp.arg = arg_esp;
pontos_esp.w = w_esp;
save('FRA.mat','pontos_esp');