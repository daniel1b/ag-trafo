classdef bipolo_class
    properties
        tipo, valor, no, noControle, nome;
    end
    properties(Constant)
        IND = 1;
        CAP = 2;
        RES = 3;
        Vin = 4;
        Iin = 5;
        VdepCT = 6;
        VdepCI = 7;
        IdepCT = 8;
        IdepCI = 9;
    end
    methods        
        function obj = bipolo_class(nome,tipo,no,valor,noControle)
            obj.nome = nome;
            obj.tipo = tipo;
            obj.no = no;
            obj.valor = valor;  
            obj.noControle = noControle;
        end        
        function truefalse = isImped(obj)
            truefalse = or(or(obj.tipo==obj.IND,obj.tipo==obj.CAP),obj.tipo==obj.RES);
        end
        function truefalse = isFonteIn(obj)
            truefalse = or(obj.tipo==obj.Vin,obj.tipo==obj.Iin);
        end
        function truefalse = isFtensao(obj)
            truefalse = or(or(obj.tipo==obj.Vin,obj.tipo==obj.VdepCT),obj.tipo==obj.VdepCI);
        end
        function truefalse = pertenceNo(obj,valor,no)
            truefalse = 0;
            for i = 1:length(no)
                if valor==no(i)
                    truefalse = 1;
                end
            end
        end
    end
end