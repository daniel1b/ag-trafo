function [l1,l2] = mov_rot(dadosx,dadosy,mov,rot)
    l1(length(dadosx)) = 0;
    l2(length(dadosx)) = 0;
    for n = 1:length(dadosx)
        dref = sqrt(power(dadosx(n),2)+power(dadosy(n),2));
        ang = atan2(dadosy(n),dadosx(n));
        
        if(isnan(ang))
            l1(n) = mov(1) + dref*cos(rot);
            l2(n) = mov(2) + dref*sin(rot);
        else
            l1(n) = mov(1) + dref*cos(rot+ang);
            l2(n) = mov(2) + dref*sin(rot+ang);
        end

    end
end