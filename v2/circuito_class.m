classdef circuito_class
    properties
        in,out,nos,bipolos,tensoes,correntes,...
            matriz_admitancia,matris_corrente,matriz_tensao
    end
    methods
        function obj = circuito_class()
            obj.in = [0 1];
            obj.out = [0 1];
        end
        function obj = add_bipolo(obj,bipolo)
            obj.bipolos = [obj.bipolos bipolo];
        end
        
        function bips = get_bipolos1(obj,a)
            bips = [];
            for i = 1:length(obj.bipolos)
                if or(obj.bipolos(i).no(1)==a,obj.bipolos(i).no(2)==a)
                    bips = [bips obj.bipolos(i)];
                end
            end
        end
        function bips = get_bipolos2(obj,a,b)
            bips = [];
            for i = 1:length(obj.bipolos)
                if and(or(obj.bipolos(i).no(1)==a,obj.bipolos(i).no(2)==a),...
                        or(obj.bipolos(i).no(1)==b,obj.bipolos(i).no(2)==b))
                    bips = [bips obj.bipolos(i)];
                end
            end
        end
        function tensoes = get_tensoes(circuito1,w)
            %calculo de Tensao nos nos            
            %indentifica nos // depois coluca tudo em uma lista e adiciona os nao
            %repetidos acho que fica mais rapido
            for i = 1:length(circuito1.bipolos)
                for m = 1:2
                    controle = 1;
                    for k = 1:length(circuito1.nos)
                        if(circuito1.bipolos(i).no(m)==circuito1.nos(k))
                            controle = 0;
                        end
                    end
                    if controle
                        circuito1.nos = [circuito1.nos circuito1.bipolos(i).no(m)];
                    end
                end
            end
            
            %agrupa os nos para formar SuperN�, mas so pega os que vao ser
            %calculados
            
            superNos = {};
            %adicionar os nos das fontes de tensao
            for i = 1:length(circuito1.bipolos)
                if circuito1.bipolos(i).isFtensao()
                    controlePrincipal = 1;
                    for k = 1:length(superNos)
                        controleA = 1;
                        controleB = 1;
                        for m = 1:length(superNos{k})
                            if superNos{k}(m) == circuito1.bipolos(i).no(1);
                                controleA = 0;
                            end
                            if superNos{k}(m) == circuito1.bipolos(i).no(2);
                                controleB = 0;
                            end
                        end
                        if and(controleA, not(controleB))
                            superNos{k}(length(superNos{k})+1) = circuito1.bipolos(i).no(1);
                            controlePrincipal = 0;
                        elseif and(controleB, not(controleA))
                            superNos{k}(length(superNos{k})+1) = circuito1.bipolos(i).no(2);
                            controlePrincipal = 0;
                        elseif and(not(controleB), not(controleA))
                            controlePrincipal = 0;
                        end
                    end
                    if controlePrincipal
                        superNos(length(superNos)+1) = {circuito1.bipolos(i).no};
                    end
                end
            end
            %adicionar o resto dos nos que nao foram add
            for i = 1:length(circuito1.bipolos)
                if not(circuito1.bipolos(i).isFtensao())
                    controleA = 1;
                    controleB = 1;
                    for k = 1:length(superNos)
                        for m = 1:length(superNos{k})
                            if superNos{k}(m) == circuito1.bipolos(i).no(1);
                                controleA = 0;
                            end
                            if superNos{k}(m) == circuito1.bipolos(i).no(2);
                                controleB = 0;
                            end
                        end
                    end
                    if controleB
                        superNos(length(superNos)+1) = {circuito1.bipolos(i).no(2)};
                    end
                    if controleA
                        superNos(length(superNos)+1) = {circuito1.bipolos(i).no(1)};
                    end
                end
            end
            % nao ha equacao para o GND que � o n� 0, entao retirar
            auxiliar = {};
            for i = 1:length(superNos)
                controle = 1;
                for j = 1:length(superNos{i})
                    if superNos{i}(j)==0
                        controle = 0;
                    end
                end
                if controle
                    auxiliar(length(auxiliar)+1) = superNos(i);
                end
            end
            superNos = auxiliar;
            
            %criar as matrizes
            m_admitancia = zeros(length(circuito1.nos)-1);
            m_corrente = zeros(length(circuito1.nos)-1,1);
            
            %comecar a preencher matrizes
            %prencher com equacoes de superno
            for i = 1:length(superNos)
                %buscar bipolos conectados ao superno
                for m = 1:length(superNos{i})
                    bips = circuito1.get_bipolos1(superNos{i}(m));
                    for j = 1:length(bips)
                        %diferencia fontes de corrente de resistencia
                        if bips(j).isImped
                            %se for impedancia
                            %calcular o valor da impedancia
                            switch(bips(j).tipo)
                                case bips(j).IND
                                    valor = bips(j).valor*1i*w;
                                case bips(j).CAP
                                    valor = 1/(bips(j).valor*1i*w);
                                case bips(j).RES
                                    valor = bips(j).valor;
                            end
                            valor
                            %saber qual sera negativo bips(j).no(1) ou bips(j).no(2)
                            switch (bips(j).no(1))
                                case superNos{i}
                                    if bips(j).no(1) ~= 0 %se for o gnd pula
                                        m_admitancia(i,bips(j).no(1)) = m_admitancia(i,bips(j).no(1)) + 1/valor;
                                    end
                                    if bips(j).no(2) ~= 0 %se for o gnd pula
                                        m_admitancia(i,bips(j).no(2)) = m_admitancia(i,bips(j).no(2)) - 1/valor;
                                    end
                                otherwise
                                    if bips(j).no(1) ~= 0 %se for o gnd pula
                                        m_admitancia(i,bips(j).no(1)) = m_admitancia(i,bips(j).no(1)) - 1/valor;
                                    end
                                    if bips(j).no(2) ~= 0 %se for o gnd pula
                                        m_admitancia(i,bips(j).no(2)) = m_admitancia(i,bips(j).no(2)) + 1/valor;
                                    end
                            end
                        else
                            %se for fonte de corrente
                            switch (bips(j).tipo)
                                case bipolo_class.Iin
                                    %se for uma fonte de corrente independente
                                    switch (bips(j).no(1))
                                        case superNos{i} %corrente saindo do no
                                            m_corrente(i,1) = m_corrente(i,1) - bips(j).valor;
                                        otherwise % corrente entrando no no
                                            m_corrente(i,1) = m_corrente(i,1) + bips(j).valor;
                                    end
                                case bipolo_class.IdepCI
                                    %se for uma fonte de corrente contr. corrente
                                    switch (bips(j).no(1))
                                        case superNos{i} %corrente saindo do no
                                            valor = bips(j).valor/bips(j).noControle.valor;
                                        otherwise % corrente entrando no no
                                            valor = -bips(j).valor/bips(j).noControle.valor;
                                    end
                                    if bips(j).noControle.no(1) ~= 0 %se for o gnd pula
                                        m_admitancia(i,bips(j).noControle.no(1)) = m_admitancia(i,bips(j).noControle.no(1)) + valor;
                                    end
                                    if bips(j).noControle.no(2) ~= 0 %se for o gnd pula
                                        m_admitancia(i,bips(j).noControle.no(2)) = m_admitancia(i,bips(j).noControle.no(2)) - valor;
                                    end
                                case bipolo_class.IdepCT
                                    %se for uma fonte de corrente contr. tensao
                                    switch (bips(j).no(1))
                                        case superNos{i} %corrente saindo do no
                                            valor = bips(j).valor;
                                        otherwise % corrente entrando no no
                                            valor = -bips(j).valor;
                                    end
                                    if bips(j).noControle.no(1) ~= 0 %se for o gnd pula
                                        m_admitancia(i,bips(j).noControle.no(1)) = m_admitancia(i,bips(j).noControle.no(1)) - valor;
                                    end
                                    if bips(j).noControle.no(2) ~= 0 %se for o gnd pula
                                        m_admitancia(i,bips(j).noControle.no(2)) = m_admitancia(i,bips(j).noControle.no(2)) + valor;
                                    end
                            end
                        end
                    end
                end
            end
            %preencher com equacoes de fontes de tensao
            offset = length(superNos)+1;
            for i = 1:length(circuito1.bipolos)
                switch (circuito1.bipolos(i).tipo)
                    case bipolo_class.Vin
                        %se for fonte de tensao independentte
                        m_corrente(offset,1) = circuito1.bipolos(i).valor;
                        if circuito1.bipolos(i).no(2) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).no(2)) =  m_admitancia(offset,circuito1.bipolos(i).no(2)) + 1;
                        end
                        if circuito1.bipolos(i).no(1) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).no(1)) =  m_admitancia(offset,circuito1.bipolos(i).no(1)) - 1;
                        end
                        offset = offset +1;
                    case bipolo_class.VdepCT
                        %se for fonte de tensao contr. por tensao
                        if circuito1.bipolos(i).no(2) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).no(2)) =  m_admitancia(offset,circuito1.bipolos(i).no(2)) + 1;
                        end
                        if circuito1.bipolos(i).no(1) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).no(1)) =  m_admitancia(offset,circuito1.bipolos(i).no(1)) - 1;
                        end
                        
                        if circuito1.bipolos(i).noControle.no(2) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).noControle.no(2)) =  ...
                                m_admitancia(offset,circuito1.bipolos(i).noControle.no(2)) - circuito1.bipolos(i).valor;
                        end
                        if circuito1.bipolos(i).noControle.no(1) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).noControle.no(1)) =  ...
                                m_admitancia(offset,circuito1.bipolos(i).noControle.no(1)) + circuito1.bipolos(i).valor;
                        end
                        offset = offset +1;
                    case bipolo_class.VdepCI
                        %se for fonte de tensao contr. cor corrente
                        if circuito1.bipolos(i).no(2) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).no(2)) =  m_admitancia(offset,circuito1.bipolos(i).no(2)) + 1;
                        end
                        if circuito1.bipolos(i).no(1) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).no(1)) =  m_admitancia(offset,circuito1.bipolos(i).no(1)) - 1;
                        end
                        
                        if circuito1.bipolos(i).noControle.no(2) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).noControle.no(2)) =  ...
                                m_admitancia(offset,circuito1.bipolos(i).noControle.no(2)) + circuito1.bipolos(i).valor/circuito1.bipolos(i).noControle.valor;
                        end
                        if circuito1.bipolos(i).noControle.no(1) ~= 0
                            m_admitancia(offset,circuito1.bipolos(i).noControle.no(1)) =  ...
                                m_admitancia(offset,circuito1.bipolos(i).noControle.no(1)) - circuito1.bipolos(i).valor/circuito1.bipolos(i).noControle.valor;
                        end
                        offset = offset +1;
                end
            end
            tensoes = m_admitancia\m_corrente;
        end
    end
end