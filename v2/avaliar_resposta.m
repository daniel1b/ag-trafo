function nota = avaliar_resposta(circuito,pontos)
nota = 0;
V1 = bipolo_class('Vin',bipolo_class.Vin,circuito.in,1,[]);
circuito = circuito.add_bipolo(V1);


w = pontos.w;
mod = 1:length(w);
arg = 1:length(w);

for k = 1:length(w)
    tensoes = circuito.get_tensoes(w(k));
    if and(circuito.in(1) ~= 0,circuito.in(2) ~= 0)
        mod(k) = 20*log10(abs(tensoes(circuito.out(2),1)-tensoes(circuito.out(1),1)));
        arg(k) = (180/pi)*angle(tensoes(circuito.out(2),1)-tensoes(circuito.out(1),1));
    elseif circuito.in(1) ~= 0
        mod(k) = 20*log10(abs(-tensoes(circuito.out(1),1)));
        arg(k) = (180/pi)*angle(-tensoes(circuito.out(1),1));
    elseif circuito.in(2) ~= 0
        mod(k) = 20*log10(abs(tensoes(circuito.out(2),1)));
        arg(k) = (180/pi)*angle(tensoes(circuito.out(2),1));
    end
end
% 
figure(2);
subplot(2,1,1);
semilogx(w,mod);
ylim([-80 0]);
subplot(2,1,2);
semilogx(w,arg);
ylim([-180 0]);
for k = 1:length(w)
    nota = nota - abs(mod(k)-pontos.mod(k));
    nota = nota - abs(arg(k)-pontos.arg(k));
end

end