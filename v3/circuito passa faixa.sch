*version 9.1 251689193
u 90
U? 2
R? 4
C? 3
V? 3
@libraries
@analysis
.AC 1 3 0
+0 1001
+1 0.1
+2 1.00K
.OP 0 
@targets
@attributes
@translators
a 0 u 13 0 0 0 hln 100 PCBOARDS=PCB
a 0 u 13 0 0 0 hln 100 PSPICE=PSPICE
a 0 u 13 0 0 0 hln 100 XILINX=XILINX
@setup
unconnectedPins 0
connectViaLabel 0
connectViaLocalLabels 0
NoStim4ExtIFPortsWarnings 1
AutoGenStim4ExtIFPorts 1
@index
pageloc 1 0 3262 
@status
n 0 116:08:16:22:12:42;1474074762 e 
s 2832 116:08:16:22:12:45;1474074765 e 
*page 1 0 970 720 iA
@ports
port 8 GND_EARTH 190 220 h
@parts
part 2 OPAMP 260 180 U
a 0 sp 11 0 50 60 hln 100 PART=OPAMP
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=U1
a 0 ap 9 0 14 0 hln 100 REFDES=U1
part 56 VAC 90 150 h
a 0 sp 0 0 0 50 hln 100 PART=VAC
a 0 a 0:13 0 0 0 hln 100 PKGREF=V2
a 1 ap 9 0 20 10 hcn 100 REFDES=V2
a 0 u 13 0 -9 23 hcn 100 ACMAG=1V
part 3 r 150 140 u
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R1
a 0 ap 9 0 15 0 hln 100 REFDES=R1
a 0 u 13 0 15 25 hln 100 VALUE=4.8679k
part 55 r 220 140 v
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R3
a 0 ap 9 0 15 0 hln 100 REFDES=R3
a 0 u 13 0 15 50 hln 100 VALUE=25.636k
part 6 r 160 190 v
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R2
a 0 ap 9 0 15 0 hln 100 REFDES=R2
a 0 u 13 0 15 30 hln 100 VALUE=200
part 4 c 170 140 h
a 0 sp 0 0 0 10 hlb 100 PART=c
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=CK05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=C1
a 0 ap 9 0 15 0 hln 100 REFDES=C1
a 0 u 13 0 15 25 hln 100 VALUE=125n
part 5 c 170 100 h
a 0 sp 0 0 0 10 hlb 100 PART=c
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=CK05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=C2
a 0 ap 9 0 15 0 hln 100 REFDES=C2
a 0 u 13 0 15 25 hln 100 VALUE=4.3053u
part 1 titleblk 970 720 h
a 1 s 13 0 350 10 hcn 100 PAGESIZE=A
a 1 s 13 0 180 60 hcn 100 PAGETITLE=
a 1 s 13 0 300 95 hrn 100 PAGENO=1
a 1 s 13 0 340 95 hrn 100 PAGECOUNT=1
@conn
w 83
a 0 up 0:33 0 0 0 hln 100 V=
s 260 180 190 180 82
s 190 180 190 190 84
s 90 190 160 190 86
a 0 up 33 0 125 189 hct 100 V=
s 190 190 190 220 89
s 160 190 190 190 88
w 58
a 0 sr 0 0 0 0 hln 100 LABEL=n1
a 0 up 0:33 0 0 0 hln 100 V=
s 90 140 110 140 57
a 0 sr 3 0 100 138 hcn 100 LABEL=n1
a 0 up 33 0 100 139 hct 100 V=
s 90 150 90 140 59
w 62
a 0 sr 0 0 0 0 hln 100 LABEL=n2
a 0 up 0:33 0 0 0 hln 100 V=
s 170 100 160 100 72
a 0 sr 3 0 165 98 hcn 100 LABEL=n2
a 0 up 33 0 165 99 hct 100 V=
s 170 140 160 140 63
s 160 140 150 140 67
s 160 150 160 140 65
s 160 100 160 140 74
w 69
a 0 sr 0 0 0 0 hln 100 LABEL=n3
a 0 up 0:33 0 0 0 hln 100 V=
s 200 140 220 140 68
a 0 sr 3 0 210 138 hcn 100 LABEL=n3
a 0 up 33 0 210 139 hct 100 V=
s 220 140 260 140 76
w 71
a 0 sr 0 0 0 0 hln 100 LABEL=n4
a 0 up 0:33 0 0 0 hln 100 V=
s 220 100 340 100 78
a 0 sr 3 0 280 98 hcn 100 LABEL=n4
a 0 up 33 0 280 99 hct 100 V=
s 220 100 200 100 70
s 340 100 340 160 80
@junction
j 90 150
+ p 56 +
+ w 58
j 200 140
+ p 4 2
+ w 69
j 200 100
+ p 5 2
+ w 71
j 260 140
+ p 2 -
+ w 69
j 340 160
+ p 2 OUT
+ w 71
j 260 180
+ p 2 +
+ w 83
j 190 220
+ s 8
+ w 83
j 90 190
+ p 56 -
+ w 83
j 190 190
+ w 83
+ w 83
j 170 140
+ p 4 1
+ w 62
j 160 140
+ w 62
+ w 62
j 170 100
+ p 5 1
+ w 62
j 110 140
+ p 3 2
+ w 58
j 150 140
+ p 3 1
+ w 62
j 160 150
+ p 6 2
+ w 62
j 160 190
+ p 6 1
+ w 83
j 220 140
+ p 55 1
+ w 69
j 220 100
+ p 55 2
+ w 71
@attributes
a 0 s 0:13 0 0 0 hln 100 PAGETITLE=
a 0 s 0:13 0 0 0 hln 100 PAGENO=1
a 0 s 0:13 0 0 0 hln 100 PAGESIZE=A
a 0 s 0:13 0 0 0 hln 100 PAGECOUNT=1
@graphics
