function erro = avaliaCKT(f,S)
fdes = 100;
S = full(S);
[vm,ii] = max(abs(S));
ia = find((abs(S)>=vm-0.2*vm),1,'first');
ib = find((abs(S)>=vm-0.1*vm),1,'last');



erro = 0.9*abs(fdes - f(ii))+0.1*abs(-f(ia) + f(ib));

end