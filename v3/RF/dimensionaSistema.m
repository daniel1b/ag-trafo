function [A,b,nnos,sA,nf] = dimensionaSistema(cir,f)
nnos = 0;
nadic = 0;
nsondas = 0;
for ia = 1:numel(cir)
    if isequal(cir{ia}{1},'FT')
        nadic = nadic+1;
        nnos = max([cir{ia}{3} cir{ia}{4} nnos]);
    elseif isequal(cir{ia}{1},'FC')
        nnos = max([cir{ia}{3} cir{ia}{4} nnos]);
    elseif isequal(cir{ia}{1},'R')
        nnos = max([cir{ia}{3} cir{ia}{4} nnos]);
    elseif isequal(cir{ia}{1},'L')
        nadic = nadic+1;
        nnos = max([cir{ia}{3} cir{ia}{4} nnos]);
    elseif isequal(cir{ia}{1},'C')
        nnos = max([cir{ia}{3} cir{ia}{4} nnos]);
    elseif isequal(cir{ia}{1},'V')
        nsondas = nsondas+1;
    elseif isequal(cir{ia}{1},'A')
        nsondas = nsondas+1;
        nadic = nadic+1;
        nnos = max([cir{ia}{3} cir{ia}{4} nnos]);
    elseif isequal(cir{ia}{1},'TRI')
        nadic = nadic+1;
        nnos = max([cir{ia}{3} cir{ia}{4} cir{ia}{5} cir{ia}{6} nnos]);
    elseif isequal(cir{ia}{1},'AOp')
        nadic = nadic+1;
        nnos = max([cir{ia}{3} cir{ia}{4} cir{ia}{5} cir{ia}{6} nnos]);
    else
        disp('Elemento inexistente!!!');
        return;
    end
end
nf = numel(f); % n�mero de frequ�ncias de an�lise
sA = (nnos+nadic)*nf; % tamanho do sistema linear
% Ax=b
clear A
A = sparse(sA,sA);
b = sparse(sA,1);
end