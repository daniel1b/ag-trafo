function Sonda = simulaRF(cir,f,plota)

[A,b,nnos,sA,nf] = dimensionaSistema(cir,f);
[A,b,Sonda] = preencheSistema(cir,A,b,nnos,sA,nf,f);
Sonda = resolveSistema(A,b,Sonda,f,plota);

end