function Sonda = resolveSistema(A,b,Sonda,f,plota)

x = A\b;

for ia = 1:numel(Sonda)
    s = Sonda(ia).Incid * x;
    Sonda(ia).Freq = f;
    Sonda(ia).Resp = s;
    
    if plota
        figure;
        set(gcf, 'WindowStyle', 'docked');
        subplot(2,1,1);
        semilogx(f,abs(s)); grid on;
        xlabel('Frequ�ncia (Hz)');
        ylabel([Sonda(ia).nome ', M�dulo']);
        subplot(2,1,2);
        semilogx(f,angle(s)*180/pi); grid on;
        xlabel('Frequ�ncia (Hz)');
        ylabel([Sonda(ia).nome ', Fase']);
    end
end