clear all; close all; format short e; clc;

% % cir = {
% % {'FT' 'FT1' 1 0 10 0}
% % {'L' 'L1' 1 2 1e-3}
% % {'C' 'C1' 2 3 1e-6}
% % {'R' 'R1' 3 0 100.0}
% % {'V' 'V_1' 1 2}
% % {'V' 'V_2' 2 3}
% % {'V' 'V_3' 3 0}
% % };

% % cir = {
% % {'FT' 'FT1' 1 0 30 0}
% % {'R' 'R1' 1 2 10.0}
% % {'L' 'L1' 2 3 5.0}
% % {'TRI' 'TRI1' 3 0 4 0 2/3}
% % {'L' 'L2' 4 0 4.0}
% % {'R' 'R2' 4 0 12.0}
% % {'V' 'V_R' 4 0}
% % };
% % f = 1/pi;

% % cir = {
% % {'FT' 'FT1' 1 0 1 0}
% % {'R' 'R1' 1 2 20e3*[0.1 10]}
% % {'C' 'C1' 2 3 1.25e-6*[0.1 10]}
% % {'C' 'C2' 3 4 15.625e-9*[0.1 10]}
% % {'R' 'R2' 3 4 400e3*[0.1 10]}
% % {'AOp' 'A1' 0 3 4 0}
% % {'V' 'V_0' 4 0}
% % };
% % avaliacao.funcao = @avaliaCKT;
% % avaliacao.sondas = {'V_0'};
% % 
% % 
% % 
% % f = 10.^linspace(0,6,500);
% % plota = 1;
% % 
% % sondas = simulaRF(cir,f,plota);

% % t = [];
% % for j = 1:10:51
% %     j
% %     cir = {};
% %     cir = {
% %         {'FT' 'FT1' 1 0 10 0}};
% %     for i = 1:j
% %         cir{end+1} = {'L' 'L1' i i+1 1e-3};
% %         cir{end+1} = {'C' 'C1' i+1 0 1e-6};
% %     end
% %     cir{end+1} = {'R' 'R1' i+1 0 100.0};
% %     cir{end+1} = {'V' 'V_R' i+1 0};
% %     
% %     f = 10.^linspace(0,6,100);
% %     plota = 0;
% %     t0 = cputime;
% %     sondas = simulaRF(cir,f,plota);
% %     t(2,end+1) = cputime - t0;
% %     t(1,end) = j;
% % end
% % 
% % plot(t(1,:),t(2,:));

tic
cir = {
{'FT' 'FT1' 1 0 1 0}
{'R' 'R1' 1 2 2e3*[0.1 5]}
{'R' 'R2' 0 2 2e3*[4 5]}
{'C' 'C1' 2 3 1.25e-6*[0.1 3]}
{'C' 'C2' 2 4 15.625e-6*[0.1 3]}
{'R' 'R3' 3 4 40e3*[0.01 3]}
{'AOp' 'A1' 0 3 4 0}
{'V' 'V_0' 4 0}
};
avaliacao.funcao = @avaliaCKT;
avaliacao.sondas = {'V_0'};

f = 10.^linspace(-1,3,200);
plota = 0;
[cirCorr,erroCorr] = otimizaRF(cir,f,plota,avaliacao);

plota = 1;
sondas = simulaRF(cirCorr,f,plota);

toc
