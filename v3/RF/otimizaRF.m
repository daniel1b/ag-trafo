function [cirCorr,erroCorr] = otimizaRF(cir,f,plota,avaliacao)

[A,b,nnos,sA,nf] = dimensionaSistema(cir,f);

cirCorr = cir;
for ia = 1:numel(cir)
    if ismember(cir{ia}{1},{'R' 'L' 'C'})
        if numel(cir{ia}{5}) > 1
            lims = cir{ia}{5};
            cirCorr{ia}{5} = lims(1) + rand(1)*(lims(2)-lims(1));
        end
    end
end
[A,b,Sonda] = preencheSistema(cirCorr,A,b,nnos,sA,nf,f);
Sonda = resolveSistema(A,b,Sonda,f,plota);
S = [];
for ib = 1:numel(avaliacao.sondas)
    for ic = 1:numel(Sonda)
        if isequal(Sonda(ic).nome,avaliacao.sondas{ib})
            S(end+1,:) = Sonda(ic).Resp;
        end
    end
end
erroCorr = feval(avaliacao.funcao,f,S);

while 1
    cirAlt = cir;
    for ia = 1:numel(cir)
        if ismember(cir{ia}{1},{'R' 'L' 'C'})
            if numel(cir{ia}{5}) > 1
                lims = cir{ia}{5};
                cirAlt{ia}{5} = cirCorr{ia}{5} + 0.1*randn(1)*(lims(2)-lims(1));
                if cirAlt{ia}{5} < lims(1)
                    cirAlt{ia}{5} = lims(1);
                elseif cirAlt{ia}{5} > lims(2)
                    cirAlt{ia}{5} = lims(2);
                end
            end
        end
    end
    [A,b,Sonda] = preencheSistema(cirAlt,A,b,nnos,sA,nf,f);
    Sonda = resolveSistema(A,b,Sonda,f,plota);
    S = [];
    for ib = 1:numel(avaliacao.sondas)
        for ic = 1:numel(Sonda)
            if isequal(Sonda(ic).nome,avaliacao.sondas{ib})
                S(end+1,:) = Sonda(ic).Resp;
            end
        end
    end
    erroAlt = feval(avaliacao.funcao,f,S);
    
    if erroAlt < erroCorr
        [~,ii] = max(S); 
        fprintf('freq max -> %f hertz   erro-> %f\n',f(ii),erroAlt);
        cirCorr = cirAlt;
        erroCorr = erroAlt;
        if erroCorr<3
           break;
        end
    end
    
end


end