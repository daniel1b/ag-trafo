* Schematics Aliases *

.ALIASES
E_U1            U1(OUT=n4 +=0 -=n3 )
V_V2            V2(+=n1 -=0 )
R_R1            R1(1=n2 2=n1 )
R_R3            R3(1=n3 2=n4 )
R_R2            R2(1=0 2=n2 )
C_C1            C1(1=n2 2=n3 )
C_C2            C2(1=n2 2=n4 )
_    _(n3=n3)
_    _(n4=n4)
_    _(n1=n1)
_    _(n2=n2)
.ENDALIASES

