clc; clear; format shortg;
circuito1 = circuito_class();

%-------cria os bipolos-----------------------------
tensao = bipolo_class(bipolo_class.Vsource,[1 2],10);

r1 = bipolo_class(bipolo_class.RES,[2 7],10);
r2 = bipolo_class(bipolo_class.RES,[2 3],10);
r3 = bipolo_class(bipolo_class.RES,[3 7],10);
r4 = bipolo_class(bipolo_class.RES,[7 1],10);
r5 = bipolo_class(bipolo_class.RES,[5 6],10);

l1 = bipolo_class(bipolo_class.IND,[4 5],10);

c1 = bipolo_class(bipolo_class.CAP,[3 4],10);
c2 = bipolo_class(bipolo_class.CAP,[4 7],10);
c3 = bipolo_class(bipolo_class.CAP,[6 1],10);

%-----------adiciona os bipolos no circuito---------

circuito1 = circuito1.add_bipolo(tensao);
circuito1 = circuito1.add_bipolo(r1);
circuito1 = circuito1.add_bipolo(r2);
circuito1 = circuito1.add_bipolo(r3);
circuito1 = circuito1.add_bipolo(r4);
circuito1 = circuito1.add_bipolo(r5);
circuito1 = circuito1.add_bipolo(l1);
circuito1 = circuito1.add_bipolo(c1);
circuito1 = circuito1.add_bipolo(c2);
circuito1 = circuito1.add_bipolo(c3);

impedancia_thev = circuito.calc_imp_thev_no();










