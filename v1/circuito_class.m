classdef circuito_class
    properties
        nos,bipolos,tensoes,correntes,...
            matriz_admitancia,matris_corrente,matriz_tensao
    end
    methods        
        function obj = circuito_class()
        end
        function obj = add_bipolo(obj,bipolo)
            obj.bipolos = [obj.bipolos bipolo];
        end
        
        function bips = get_bipolos1(obj,a)
            bips = [];
            for i = 1:length(obj.bipolos)
                if or(obj.bipolos(i).no(1)==a,obj.bipolos(i).no(2)==a)
                    bips = [bips obj.bipolos(i)];
                end
            end
        end
        function bips = get_bipolos2(obj,a,b)
            bips = [];
            for i = 1:length(obj.bipolos)
                if and(or(obj.bipolos(i).no(1)==a,obj.bipolos(i).no(2)==a),...
                        or(obj.bipolos(i).no(1)==b,obj.bipolos(i).no(2)==b))
                    bips = [bips obj.bipolos(i)];
                end
            end
        end
        
        function obj = calc_mat_admit(obj)
            %listar todos os nos
            for i = 1:length(obj.bipolos)
                a = obj.bipolos(i).no(1);
                b = obj.bipolos(i).no(2);
                add_a = 1;
                add_b = 1;
                for j = 1:length(obj.nos)
                    if obj.nos(j)==a
                        add_a = 0;
                    end
                    if obj.nos(j)==b
                        add_b = 0;
                    end                    
                end
                if add_a
                    obj.nos = [obj.nos a];
                end
                if add_b
                    obj.nos = [obj.nos b];
                end
            end
            %criar matriz
            obj.matriz_admitancia = zeros(length(obj.nos)-1);
            %iterar matriz
            for i = 1:length(obj.nos)-1
                for j = 1:length(obj.nos)-1
                    if i==j
                        bips = obj.get_bipolos1(obj.nos(i));
                        valor = 0;
                        for k = 1:length(bips)
                            if bips(k).isImped()
                                valor = valor + bips(k).admitancia;
                            end
                        end
                        obj.matriz_admitancia(i,j) = valor;
                    else
                        bips = obj.get_bipolos2(obj.nos(i),obj.nos(j));
                        valor = 0;
                        for k = 1:length(bips)
                            if bips(k).isImped()
                                valor = valor + bips(k).admitancia;
                            end
                        end
                        obj.matriz_admitancia(i,j) = -valor;    
                    end
                end
            end
        end
        function circuito = remove_fontes_dependentes(obj)
            circuito = circuito_class();
            for i = 1:length(obj.bipolos)
                if obj.bipolos(i).isImped();
                    circuito.add_bipolo(obj.bipolos(i));
                end
            end
        end
        function impedancia = calc_imp_thev_no(obj,noA,noB)
            circuito = obj.remove_fontes_dependentes();
            tensao = bipolo_class(bipolo_class.Vsource,[noA noB],1);
            circuito = circuito.add_bipolo(tensao);
            
            
        end
    end
end