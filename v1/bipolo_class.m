classdef bipolo_class
    properties
        tipo, tensao, corrente, no, impedancia, admitancia;
    end
    properties(Constant)
        IND = 1;
        CAP = 2;
        RES = 3;
        Vsource = 4;
        Isource = 5;
    end
    methods        
        function obj = bipolo_class(tipo,no,valor)
            obj.tipo = tipo;
            obj.no = no;
            if tipo==obj.IND
                obj.impedancia = valor*1i; 
                obj.admitancia = 1/obj.impedancia;
            elseif tipo==obj.CAP
                obj.impedancia = 1/(valor*1i); 
                obj.admitancia = 1/obj.impedancia;
            elseif tipo==obj.RES
                obj.impedancia = valor;
                obj.admitancia = 1/obj.impedancia;
            elseif tipo==obj.Vsource
                syms s;
                obj.tensao = valor;
            elseif tipo==obj.Isource  
                obj.corrente = valor;
            end            
        end
        function truefalse = isImped(obj)
                truefalse = or(or(obj.tipo==obj.IND,obj.tipo==obj.CAP),obj.tipo==obj.RES);
            end
    end
end