clear all; close all; format short e; clc;
% tic 
% cir = {
% {'FT' 'FT1' 1 0 5.94 140}
% {'R' 'R1' 1 2 5}
% 
% {'L' 'L1' 3 2 4}
% {'L' 'L2' 0 3 5}
% 
% {'M' 'L1' 'L2' 2}
% 
% {'V' 'V0' 3 0}
% };
% 
% f = 10.^linspace(-1,3,200);
% plota = 1;
% sondas = simulaRF(cir,f,plota);
% toc

cir1 = {
{'FT' 'FT1' 1 0 10 0}
{'R' 'R1' 1 2 5}
{'R' 'R2' 0 2 5}

{'L' 'L1' 3 2 2}
{'L' 'L2' 4 3 2}
{'L' 'L3' 4 0 3}

{'M' 'L1' 'L2' 0.5}
{'M' 'L2' 'L3' 1}
{'M' 'L3' 'L1' 1.5}

{'V' 'V0' 2 0}
};

cir2 = {
{'FT' 'FT1' 1 0 10 0}
{'R' 'R1' 1 2 5}
{'R' 'R2' 0 2 5}

{'L' 'L1' 0 2 3}

{'V' 'V0' 2 0}
};

f = 10.^linspace(-1,3,200);
plota = 1;
sondas1 = simulaRF(cir1,f,plota);
sondas2 = simulaRF(cir2,f,plota);

