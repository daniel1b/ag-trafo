function Sonda = simulaRF(cir,f,plota)

[nnos,sA,nf] = dimensionaSistema(cir,f);

[A,b,Sonda] = preencheSistema(cir,nnos,sA,nf,f);

Sonda = resolveSistema(A,b,Sonda,f,plota);

end