function Sonda = resolveSistema(A,b,Sonda,f,plota)


x = A\b;

x = full(x);
Sonda.Incid = full(Sonda.Incid);
f = full(f);



for ia = 1:numel(Sonda)
    s = Sonda(ia).Incid * x;
    Sonda(ia).Freq = f;
    Sonda(ia).Resp = s;
    
    if plota
        figure;
        set(gcf, 'WindowStyle', 'docked');
        subplot(2,1,1);
        if numel(f)==1
            semilogx(f,abs(s),'sq'); grid on;
        else
            semilogx(f,abs(s)); grid on;
        end
        xlabel('Frequ�ncia (Hz)');
        ylabel([Sonda(ia).nome ', M�dulo']);
        subplot(2,1,2);
        if numel(f)==1
            semilogx(f,angle(s)*180/pi,'sq'); grid on;
            fprintf('sonda %s = %f |_ %f�\n',Sonda(ia).nome,abs(s),angle(s)*180/pi)
        else
            semilogx(f,angle(s)*180/pi); grid on;
        end
        
        xlabel('Frequ�ncia (Hz)');
        ylabel([Sonda(ia).nome ', Fase']);
    end
end