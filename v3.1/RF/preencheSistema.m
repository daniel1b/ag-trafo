function [A,b,Sonda] = preencheSistema(cir,nnos,sA,nf,f)
clear A
clear b

A = sparse(sA,sA);
b = sparse(sA,1);


NomeIadic.Nome = {};
NomeIadic.Iadic = [];

iadic = nnos;
nsondas = 0;
Mutua = {};
for ia = 1:numel(cir)
    if isequal(cir{ia}{1},'FT')
        iadic = iadic+1;
        n1 = cir{ia}{3}; n2 = cir{ia}{4};
        V = cir{ia}{5}*exp(1i*cir{ia}{6}*pi/180);
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            if n1 ~= 0
                A(n1+ref,iadic+ref) = +1;
                A(iadic+ref,n1+ref) = +1;
            end
            if n2 ~= 0
                A(n2+ref,iadic+ref) = -1;
                A(iadic+ref,n2+ref) = -1;
            end
            b(iadic+ref,1) = V;
        end
    elseif isequal(cir{ia}{1},'FC')
        n1 = cir{ia}{3}; n2 = cir{ia}{4};
        I = cir{ia}{5}*exp(1i*cir{ia}{6}*pi/180);
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            if n1 ~= 0
                b(n1+ref,1) = b(n1+ref,1)-I;
            end
            if n2 ~= 0
                b(n2+ref,1) = b(n2+ref,1)+I;
            end
        end
        
    elseif isequal(cir{ia}{1},'R')
        n1 = cir{ia}{3}; n2 = cir{ia}{4};
        R = cir{ia}{5};
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            if n1 ~= 0 && n2 ~= 0
                A(n1+ref,n1+ref) = A(n1+ref,n1+ref)+1/R;
                A(n1+ref,n2+ref) = A(n1+ref,n2+ref)-1/R;
                A(n2+ref,n1+ref) = A(n2+ref,n1+ref)-1/R;
                A(n2+ref,n2+ref) = A(n2+ref,n2+ref)+1/R;
            elseif n2 == 0
                A(n1+ref,n1+ref) = A(n1+ref,n1+ref)+1/R;
            else
                A(n2+ref,n2+ref) = A(n2+ref,n2+ref)+1/R;
            end
        end
        
    elseif isequal(cir{ia}{1},'L')
        iadic = iadic+1;
        n1 = cir{ia}{3}; n2 = cir{ia}{4};
        L = cir{ia}{5};
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            if n1 ~= 0
                A(n1+ref,iadic+ref) = +1;
                A(iadic+ref,n1+ref) = +1;
            end
            if n2 ~= 0
                A(n2+ref,iadic+ref) = -1;
                A(iadic+ref,n2+ref) = -1;
            end
            A(iadic+ref,iadic+ref) = -1i*(2*pi*f(ib))*L;
        end
        NomeIadic.Nome{end+1} = cir{ia}{2};
        NomeIadic.Iadic(end+1) = iadic;
    elseif isequal(cir{ia}{1},'C')
        n1 = cir{ia}{3}; n2 = cir{ia}{4};
        C = cir{ia}{5};
        for ib = 1:nf
            Y = 1i*(2*pi*f(ib))*C;
            ref = (ib-1)*sA/nf;
            if n1 ~= 0 && n2 ~= 0
                A(n1+ref,n1+ref) = A(n1+ref,n1+ref)+Y;
                A(n1+ref,n2+ref) = A(n1+ref,n2+ref)-Y;
                A(n2+ref,n1+ref) = A(n2+ref,n1+ref)-Y;
                A(n2+ref,n2+ref) = A(n2+ref,n2+ref)+Y;
            elseif n2 == 0
                A(n1+ref,n1+ref) = A(n1+ref,n1+ref)+Y;
            else
                A(n2+ref,n2+ref) = A(n2+ref,n2+ref)+Y;
            end
        end
        
    elseif isequal(cir{ia}{1},'V')
        nsondas = nsondas+1;
        Sonda(nsondas).nome = cir{ia}{2};
        n1 = cir{ia}{3}; n2 = cir{ia}{4};
        Sonda(nsondas).Incid = sparse(nf,sA);
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            if n1 ~= 0
                Sonda(nsondas).Incid(ib,n1+ref) = +1;
            end
            if n2 ~= 0
                Sonda(nsondas).Incid(ib,n2+ref) = -1;
            end
        end
        
    elseif isequal(cir{ia}{1},'A')
        iadic = iadic+1;
        n1 = cir{ia}{3}; n2 = cir{ia}{4};
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            if n1 ~= 0
                A(n1+ref,iadic+ref) = +1;
                A(iadic+ref,n1+ref) = +1;
            end
            if n2 ~= 0
                A(n2+ref,iadic+ref) = -1;
                A(iadic+ref,n2+ref) = -1;
            end
        end
        nsondas = nsondas+1;
        Sonda(nsondas).nome = cir{ia}{2};
        Sonda(nsondas).Incid = sparse(nf,sA);
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            Sonda(nsondas).Incid(ib,iadic+ref) = +1;
        end
        
    elseif isequal(cir{ia}{1},'TRI')
        iadic = iadic+1;
        n1 = cir{ia}{3}; n2 = cir{ia}{4};
        n3 = cir{ia}{5}; n4 = cir{ia}{6};
        N = cir{ia}{7};
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            if n1 ~= 0
                A(n1+ref,iadic+ref) = +1;
                A(iadic+ref,n1+ref) = +1;
            end
            if n2 ~= 0
                A(n2+ref,iadic+ref) = -1;
                A(iadic+ref,n2+ref) = -1;
            end
            if n3 ~= 0
                A(n3+ref,iadic+ref) = -N;
                A(iadic+ref,n3+ref) = -N;
            end
            if n4 ~= 0
                A(n4+ref,iadic+ref) = +N;
                A(iadic+ref,n4+ref) = +N;
            end
        end
    elseif isequal(cir{ia}{1},'AOp')
        iadic = iadic+1;
        n1 = cir{ia}{3}; n2 = cir{ia}{4};
        n3 = cir{ia}{5}; n4 = cir{ia}{6};
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            if n1 ~= 0
                A(iadic+ref,n1+ref) = +1;
            end
            if n2 ~= 0
                A(iadic+ref,n2+ref) = -1;
            end
            if n3 ~= 0
                A(n3+ref,iadic+ref) = +1;
            end
            if n4 ~= 0
                A(n4+ref,iadic+ref) = -1;
            end
        end
    elseif isequal(cir{ia}{1},'M')
        Mutua{end+1} = cir{ia};
    end
    
    for ia = 1:numel(Mutua)
        [cond1,i1] = ismember(Mutua{ia}{2},NomeIadic.Nome);
        [cond2,i2] = ismember(Mutua{ia}{3},NomeIadic.Nome);
        if(not(cond1 && cond2))
            Mutua{ia}{:}
            disp('indutancia mutua para indutor que nao existe!!!!')
            return
        end
        n1 = NomeIadic.Iadic(i1);
        n2 = NomeIadic.Iadic(i2);
        
        for ib = 1:nf
            ref = (ib-1)*sA/nf;
            Z = 1i*(2*pi*f(ib))*Mutua{ia}{4};
            A(n1+ref,n2+ref) = -Z;
            A(n2+ref,n1+ref) = -Z;
        end
    end
end
